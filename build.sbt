lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "com.slackshack",
      scalaVersion := "2.13.1"
    )
  ),
  name := "tic-tac-toe"
)

libraryDependencies += "org.typelevel" %% "cats-core" % "2.0.0"
libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.14.1" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % Test
libraryDependencies += "org.scalamock" %% "scalamock" % "5.0.0" % Test
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"
libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.9.2"

scalacOptions ++= Seq(
  "-Xfatal-warnings",
  "-deprecation"
)

ThisBuild / useSuperShell := false
