package com.tictactoe

import org.scalatest._
import wordspec.AnyWordSpec
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.matchers.should.Matchers
import cats.data.State
import org.scalamock.scalatest.MockFactory

import TicCatToe._

class TicCatToeSpec
  extends AnyWordSpec
  with Matchers
  with Inspectors
  with MockFactory
  with LazyLogging {

  "context" when {
    "should" should {
      "playMove - winning move" in {
        playMove((board: Board) => 1)
          .runA(
            Seq(
              '1', '2', '3', //
              'O', 'X', '6', //
              'O', 'X', '9'
            ),
            Nought
          )
          .value shouldBe Won(Nought)
      }

      "playMove - 2 moves to Win" in {
        val moveReader = mockFunction[Board, Int]
        Seq(4, 2).foreach {
          moveReader.expects(*).returning(_)
        }

        playMove(moveReader)
          .runA(
            Seq(
              '1', '2', '3', //
              '4', 'X', '6', //
              'O', 'X', '9'
            ),
            Nought
          )
          .value shouldBe Won(Cross)
      }

      "playMove - full game" in {
        val moveReader = mockFunction[Board, Int]
        Seq(1, 2, 3, 4, 5, 6, 7).foreach {
          moveReader.expects(*).returning(_)
        }

        playMove(moveReader)
          .runA(
            Seq(
              '1', '2', '3', //
              '4', '5', '6', //
              '7', '8', '9'
            ),
            Nought
          )
          .value shouldBe Won(Nought)
      }

      "playMove - draw" in {
        val moveReader = mockFunction[Board, Int]
        Seq(1, 2, 3, 5, 6, 4, 8, 9, 7).foreach {
          moveReader.expects(*).returning(_)
        }

        playMove(moveReader)
          .runA(
            Seq(
              '1', '2', '3', //
              '4', '5', '6', //
              '7', '8', '9'
            ),
            Nought
          )
          .value shouldBe Draw
      }
    }
  }
}
