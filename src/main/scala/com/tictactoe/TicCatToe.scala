package com.tictactoe

import cats.data.State
import com.typesafe.scalalogging.LazyLogging
import scala.io.StdIn.readLine

object TicCatToe extends LazyLogging {

  sealed trait Player {
    val symbol: Char
  }

  case object Nought extends Player {
    val symbol = 'O'
  }

  case object Cross extends Player {
    val symbol = 'X'
  }

  sealed trait BoardStatus
  case class Won(player: Player) extends BoardStatus
  case object Draw extends BoardStatus
  case object InProgress extends BoardStatus

  type Board = Seq[Char] // immutable
  case class PlayerMove(player: Player, move: Int)

  type BoardState = (Board, Player)
  type GameState[A] = State[BoardState, A]

  type MoveReader = Board => Int

  def main(args: Array[String]): Unit = {
    playMove(moveReader)
      .runA(
        Seq(
          '1', '2', '3', //
          '4', '5', '6', //
          '7', '8', '9'
        ),
        Nought
      )
      .value match {
      case Draw        => println("its a draw")
      case Won(winner) => println(s"$winner won!")
      case InProgress  => println("Unexpectedly the game is not finished")
    }
  }

  def updateBoard(pm: PlayerMove) =
    State[BoardState, BoardStatus] { (state: BoardState) =>
      {
        val (board, _) = state
        val nb = board.updated((pm.move - 1), pm.player.symbol)
        val ns: BoardState = (nb, nextPlayer(pm.player))
        (ns, boardStatus(nb))
      }
    }

  def playMove(
      moveReader: MoveReader
  ): GameState[BoardStatus] = {
    for {
      s <- State.get[BoardState]
      (b, p) = s
      _ = printBoard(b)
      status <- updateBoard(PlayerMove(p, moveReader(b)))
      result <- status match {
        case InProgress => playMove(moveReader)
        case bs         => State.pure[BoardState, BoardStatus](bs)
      }
    } yield result
  }

  def moveReader(board: Board): Int = {
    try {
      val input = readLine("Input next Turn: ").toInt
      if (input < 1 || input > 9 || !board(input - 1).toString.matches(
            "[1-9]"
          )) {
        throw new Exception
      }
      input
    } catch {
      case _: Exception => moveReader(board)
    }
  }

  val printBoard = format _ andThen println

  def boardStatus(board: Board): BoardStatus =
    hasWinner(board) match {
      case None if (remainingTurns(board) == 0) => Draw
      case None                                 => InProgress
      case Some(winner)                         => Won(winner)
    }

  def remainingTurns(board: Board): Int =
    board.count(_.toString.matches("[1-9]"))

  def updateBoard(board: Board, pm: PlayerMove): Board =
    board.updated((pm.move - 1), pm.player.symbol) // immutable, copy

  def nextPlayer(current: Player): Player =
    current match {
      case Nought => Cross
      case Cross  => Nought
    }

  def format(board: Board): String =
    0 to 2 map { r =>
      0 to 2 map { c =>
        board(c + r * 3)
      } mkString "|"
    } mkString ("__________\n", "\n------\n", "\n")

  def hasWinner(board: Board): Option[Player] =
    Set(
      Set(0, 1, 2),
      Set(3, 4, 5),
      Set(6, 7, 8),
      Set(0, 3, 6),
      Set(1, 4, 7),
      Set(2, 5, 8),
      Set(0, 4, 8),
      Set(2, 4, 6)
    ).foldLeft[Option[Player]](None)((acc, pattern) =>
      if (pattern.forall(p => board(p) == board(pattern.head))) {
        board(pattern.head) match {
          case Nought.symbol => Some(Nought)
          case Cross.symbol  => Some(Cross)
        }
      } else {
        acc
      }
    )
}
